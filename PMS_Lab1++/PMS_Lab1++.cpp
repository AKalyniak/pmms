// PMS_Lab1++.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#include <Windows.h>
#include <gl/gl.h>
#include "GL/glut.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <string>

using namespace std;

float areaWidth = 200;
float areaHeight = 200;
int windowWidth, windowHeight;
float wallColor[3], roofColor[3], leavesColor[3];
int colorset = 0;
bool writings = true, house = true, trees = true;

void init(void)
{
	glClearColor(1, 1, 1, 1);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-areaWidth, areaWidth, 0.0, -areaHeight);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

void drawString(float x, float y, string text, void *font)
{
	glRasterPos2f(x, y);
	for (char c : text)
		glutBitmapCharacter(font, c);

}

void drawTree(float x, float y)
{
	glColor3f(leavesColor[0], leavesColor[1], leavesColor[2]);
	glLineWidth(1);
	int radius = 60;
	glBegin(GL_POLYGON);
	{
		for (double i = 0; i < 2 * M_PI; i += M_PI / 6)
		{
			glVertex3f(x + cos(i) * radius, sin(i) * radius - 50, 0.0);
		}
	}
	glEnd();
	//�����

	glColor3f(51.0 / 255.0, 25.0 / 255.0, 0);
	glLineWidth(1);
	glBegin(GL_POLYGON);
	{
		glVertex2f(x - 10, y);
		glVertex2f(x + 10, y);
		glVertex2f(x + 10, -50);
		glVertex2f(x - 10, -50);
	}
	glEnd();
	//�������
}

void drawShapes(void)
{
	glClearColor(1, 1, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	if (house)
	{
		glColor3f(wallColor[0], wallColor[1], wallColor[2]);
		glLineWidth(1);
		glBegin(GL_POLYGON);
		{
			glVertex2f(-60, -areaHeight);
			glVertex2f(-60, -areaHeight + 120);
			glVertex2f(60, -areaHeight + 120);
			glVertex2f(60, -areaHeight);
		}
		glEnd();
		//ĳ�

		glColor3f(roofColor[0], roofColor[1], roofColor[2]);
		glLineWidth(1);
		glBegin(GL_POLYGON);
		{
			glVertex2f(-70, -areaHeight + 120);
			glVertex2f(70, -areaHeight + 120);
			glVertex2f(0, -areaHeight + 160);
		}
		glEnd();
		//���
	}

	if (trees)
	{
		drawTree(-150, -areaHeight);
		drawTree(-100, -areaHeight);
		drawTree(125, -areaHeight);
		//������
	}

	if (writings)
	{
		glColor3f(0, 0, 0);
		drawString(-20, -40, "House", GLUT_BITMAP_HELVETICA_12);
		drawString(-165, 10, "Tree", GLUT_BITMAP_HELVETICA_12);
		drawString(-115, 10, "Tree", GLUT_BITMAP_HELVETICA_12);
		drawString(110, 10, "Tree", GLUT_BITMAP_HELVETICA_12);
	}

	glFlush();
}

void winReshapeFcn(GLint width, GLint height)
{
	windowWidth = width;
	windowHeight = height;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-areaWidth, areaWidth, -areaHeight, areaHeight);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);    // �������� ����
}

void TimerFunc(int value)
{
	if (colorset == 0)
	{
		wallColor[0] = 1; wallColor[1] = 0.5; wallColor[2] = 0;
		roofColor[0] = 0.5; roofColor[1] = 0.5; roofColor[2] = 0.5;
		leavesColor[0] = 1; leavesColor[1] = 0.5; leavesColor[2] = 0;
		colorset = 1;
	}
	else
	{
		wallColor[0] = 1; wallColor[1] = 1; wallColor[2] = 0;
		roofColor[0] = 1; roofColor[1] = 0; roofColor[2] = 0;
		leavesColor[0] = 0; leavesColor[1] = 1; leavesColor[2] = 0;
		colorset = 0;
	}
	glutTimerFunc(1000, TimerFunc, 0);
}

void eraseWFunc(int value)
{
	writings = false;
}

void eraseHFunc(int value)
{
	house = false;
}

void eraseTFunc(int value)
{
	trees = false;
}

void idle()
{
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_MULTISAMPLE);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(400, 400);
	glutCreateWindow("����������� ������ �1");
	init();

	wallColor[0] = 1; wallColor[1] = 1; wallColor[2] = 0;
	roofColor[0] = 1; roofColor[1] = 0; roofColor[2] = 0;
	leavesColor[0] = 0; leavesColor[1] = 1; leavesColor[2] = 0;

	glutDisplayFunc(drawShapes);
	glutReshapeFunc(winReshapeFcn);
	glutTimerFunc(1000, TimerFunc, 0);
	glutTimerFunc(5000, eraseWFunc, 0);
	glutTimerFunc(10000, eraseTFunc, 0);
	glutTimerFunc(15000, eraseHFunc, 0);
	glutIdleFunc(idle);
	glutMainLoop();
}

